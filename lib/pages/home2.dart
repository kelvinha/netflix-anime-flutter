import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_netflix_anime/pages/detail.dart';
import 'package:flutter_netflix_anime/theme.dart';
import 'package:flutter_netflix_anime/widget/card_new.dart';

import '../models/anime.dart';
import 'package:http/http.dart' as http;

class MyHome2 extends StatefulWidget {
  const MyHome2({Key? key}) : super(key: key);

  @override
  State<MyHome2> createState() => _MyHome2State();
}

class _MyHome2State extends State<MyHome2> {
  TextEditingController searchController = TextEditingController();

  List<String> newTab = [
    'All',
    'Tv',
    'Movie',
    'Ova',
    'Special',
    'Ona',
    'Music'
  ];

  late int current = 0;
  late String title;

  Future<List<Anime>> getAnime(current, title) async {
    late Uri uri;
    uri =
        Uri.parse('https://api.jikan.moe/v4/anime?sfw&limit=25&status=airing');
    if (current != 0) {
      uri = Uri.parse(
          'https://api.jikan.moe/v4/anime?sfw&limit=25&status=airing&type=${newTab[current]}');
    }

    if (title != '' && current == 0) {
      uri = Uri.parse('https://api.jikan.moe/v4/anime?q=$title');
    }

    final response = await http.get(uri);

    if (response.body.isNotEmpty) {
      final Map<String, dynamic> data2 = json.decode(response.body);
      final List<dynamic> data = data2['data'];
      // ignore: avoid_print
      // print(data);
      return data.map((item) => Anime.fromJson(item)).toList();
      // return Anime.fromJson(data['data']);
    } else {
      throw Exception('Failed to load data');
    }
  }

  // ignore: prefer_typing_uninitialized_variables
  late var listAnime;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listAnime = getAnime(0, '');
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryColor,
      body: Column(
        children: [
          Container(
            height: 170,
            width: double.infinity,
            decoration: const BoxDecoration(
              color: primaryColor,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(
                  30,
                ),
                bottomRight: Radius.circular(
                  30,
                ),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "ANIMFLIX",
                    style: textPoppins.copyWith(
                      color: baseColor,
                      fontSize: 24,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextField(
                    controller: searchController,
                    onSubmitted: (value) {
                      setState(() {
                        searchController.text = value;
                        listAnime = getAnime(0, searchController.text);
                      });
                    },
                    autofocus: false,
                    cursorColor: baseColor.withOpacity(0.7),
                    style: TextStyle(
                      color: baseColor.withOpacity(0.7),
                    ),
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(
                          20,
                        ),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(
                          20,
                        ),
                      ),
                      filled: true,
                      fillColor: const Color(0xFF8D2525),
                      hintText: 'Search',
                      hintStyle: TextStyle(
                        color: baseColor.withOpacity(0.7),
                        fontSize: 20,
                      ),
                      prefixIconColor: baseColor.withOpacity(0.7),
                      border: InputBorder.none,
                      prefixIcon: const Icon(
                        Icons.search,
                      ),
                      suffixIcon: const Icon(
                        Icons.keyboard_voice_outlined,
                      ),
                      suffixIconColor: baseColor.withOpacity(0.7),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 100,
            child: ListView.builder(
              padding: const EdgeInsets.all(10),
              scrollDirection: Axis.horizontal,
              physics: const BouncingScrollPhysics(),
              itemCount: newTab.length,
              itemBuilder: (context, index) {
                if (current == index) {
                  return TabBarActive(
                    newTab: newTab,
                    index: index,
                  );
                } else {
                  return GestureDetector(
                    onTap: () {
                      setState(() {
                        current = index;
                        listAnime = getAnime(index, '');
                      });
                      searchController.clear();
                    },
                    child: TabBarInActivated(
                      newTab: newTab,
                      index: index,
                    ),
                  );
                }
              },
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: FutureBuilder<List<Anime>>(
                future: listAnime,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Center(
                      child: CircularProgressIndicator(
                        color: primaryColor.withOpacity(0.5),
                      ),
                    );
                  } else if (snapshot.hasError) {
                    return Text('Error: ${snapshot.error}');
                  } else {
                    if (!snapshot.hasData) {
                      return Center(
                        child: Text("${snapshot.hasData}"),
                      );
                    } else {
                      final anime = snapshot.data!;
                      return Center(
                        child: Wrap(
                          alignment: WrapAlignment.spaceBetween,
                          children: [
                            ...anime.map(
                              (dataAnime) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                        builder: (context) => DetailAnime(
                                          id: dataAnime.id,
                                        ),
                                      ),
                                    );
                                  },
                                  child: CardNew(
                                    title: dataAnime.title,
                                    url: dataAnime.url,
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      );
                    }
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}

class TabBarInActivated extends StatelessWidget {
  const TabBarInActivated({Key? key, required this.newTab, required this.index})
      : super(key: key);

  final List<String> newTab;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      width: newTab.length * 12,
      decoration: BoxDecoration(
        border: Border.all(
          color: baseColor.withOpacity(0.3),
        ),
        color: secondaryColor.withOpacity(0.5),
        borderRadius: const BorderRadius.all(
          Radius.circular(
            20,
          ),
        ),
      ),
      child: Center(
        child: Text(
          newTab[index],
          style: textPoppins.copyWith(
            color: baseColor.withOpacity(0.3),
          ),
        ),
      ),
    );
  }
}

class TabBarActive extends StatelessWidget {
  const TabBarActive({Key? key, required this.newTab, required this.index})
      : super(key: key);

  final List<String> newTab;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      width: newTab.length * 12,
      decoration: const BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(
            20,
          ),
        ),
      ),
      child: Center(
        child: Text(
          newTab[index],
          style: textPoppins.copyWith(
            color: baseColor,
          ),
        ),
      ),
    );
  }
}
