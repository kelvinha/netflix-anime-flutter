import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_netflix_anime/models/anime.dart';
import 'package:flutter_netflix_anime/pages/detail.dart';
import 'package:flutter_netflix_anime/widget/card_new.dart';
// ignore: depend_on_referenced_packages
import 'package:http/http.dart' as http;

import '../theme.dart';
// import '../widget/card_new.dart';

class MyHome extends StatefulWidget {
  const MyHome({Key? key}) : super(key: key);

  @override
  State<MyHome> createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  Future<List<Anime>> getAnime() async {
    final uri =
        Uri.parse('https://api.jikan.moe/v4/anime?sfw&limit=25&status=airing');
    final response = await http.get(uri);

    if (response.body.isNotEmpty) {
      final Map<String, dynamic> data2 = json.decode(response.body);
      final List<dynamic> data = data2['data'];
      // ignore: avoid_print
      // print(data);
      return data.map((item) => Anime.fromJson(item)).toList();
      // return Anime.fromJson(data['data']);
    } else {
      throw Exception('Failed to load api');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: primaryColor,
        title: Text(
          "Netflix Anime",
          style: textPoppins.copyWith(
            fontSize: 16,
          ),
        ),
      ),
      body: SingleChildScrollView(
        // padding: EdgeInsets.only(left: 20),
        scrollDirection: Axis.vertical,
        child: Column(
          children: [
            FutureBuilder<List<Anime>>(
              future: getAnime(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const Center(
                    child: CircularProgressIndicator(
                      color: primaryColor,
                    ),
                  );
                } else if (snapshot.hasError) {
                  return Text('Error: ${snapshot.error}');
                } else {
                  if (!snapshot.hasData) {
                    return Center(
                      child: Text("${snapshot.hasData}"),
                    );
                  } else {
                    final anime = snapshot.data!;
                    return Center(
                      child: Wrap(
                        alignment: WrapAlignment.spaceBetween,
                        children: [
                          ...anime.map(
                            (dataAnime) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    CupertinoPageRoute(
                                      builder: (context) => DetailAnime(
                                        id: dataAnime.id,
                                      ),
                                    ),
                                  );
                                },
                                child: CardNew(
                                  title: dataAnime.title,
                                  url: dataAnime.url,
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    );
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
