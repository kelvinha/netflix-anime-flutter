import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_netflix_anime/theme.dart';
// ignore: depend_on_referenced_packages
import 'package:http/http.dart' as http;

class DetailAnime extends StatefulWidget {
  final int id;

  const DetailAnime({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  State<DetailAnime> createState() => _DetailAnimeState();
}

class _DetailAnimeState extends State<DetailAnime> {
  Future<Map<String, dynamic>> getAnime() async {
    final uri = Uri.parse('https://api.jikan.moe/v4/anime/${widget.id}');
    final response = await http.get(uri);

    if (response.body.isNotEmpty) {
      final Map<String, dynamic> data = json.decode(response.body);
      return data;
    } else {
      throw Exception('Failed to load album');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: secondaryColor,
      body: FutureBuilder<Map<String, dynamic>>(
        future: getAnime(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(
                color: primaryColor,
              ),
            );
          }
          if (!snapshot.hasData) {
            return Center(
              child: Text("${snapshot.hasData}"),
            );
          } else {
            final detail = snapshot.data!['data'];
            return SafeArea(
              child: Stack(
                children: [
                  // ignore: sized_box_for_whitespace
                  HeroImage(detail: detail),
                  // ignore: avoid_unnecessary_containers
                  const ButtonBackCustom(),
                  Align(
                    alignment: const Alignment(0, -0.85),
                    child: Container(
                      width: 150,
                      height: 150,
                      decoration: BoxDecoration(
                        color: primaryColor.withOpacity(0.6),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(150 / 2),
                        ),
                      ),
                    ),
                  ),
                  Align(
                    alignment: const Alignment(0, -0.78),
                    child: Container(
                      width: 100,
                      height: 100,
                      decoration: const BoxDecoration(
                        color: primaryColor,
                        borderRadius: BorderRadius.all(
                          Radius.circular(100 / 2),
                        ),
                      ),
                      child: const Center(
                        child: Icon(
                          Icons.play_arrow,
                          size: 50,
                          color: baseColor,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(20),
                    margin: const EdgeInsets.only(
                      top: 220,
                      left: 10,
                      right: 10,
                    ),
                    decoration: BoxDecoration(
                      color: secondaryColor,
                      borderRadius: const BorderRadius.vertical(
                        top: Radius.circular(20),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: secondaryColor.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(
                            0,
                            3,
                          ), // changes position of shadow
                        ),
                      ],
                    ),
                    width: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ImageSmall(detail: detail),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    detail['title'],
                                    style: textPoppinsBold.copyWith(
                                      fontSize: 16,
                                      color: primaryColor,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Text(
                                    detail['genres']
                                        .map<String>((genre) =>
                                            genre['name'].toString())
                                        .join(', '),
                                    style: textPoppinsBold.copyWith(
                                      color: primaryColor,
                                      fontSize: 12,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 8,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        padding: const EdgeInsets.all(5),
                                        height: 20,
                                        width: 50,
                                        decoration: const BoxDecoration(
                                          color: baseColor,
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        child: Text(
                                          detail['duration'],
                                          style: textPoppinsBold.copyWith(
                                            color: secondaryColor,
                                            fontSize: 8,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "Score : ",
                                            style: textPoppins.copyWith(
                                              color: thirdColor,
                                            ),
                                          ),
                                          Text(
                                            detail['score'].toString(),
                                            style: textPoppinsBold.copyWith(
                                              fontSize: 25,
                                              color: thirdColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 30,
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    padding: const EdgeInsets.all(5),
                                    height: 25,
                                    width: double.infinity,
                                    decoration: const BoxDecoration(
                                      color: primaryColor,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(30),
                                      ),
                                    ),
                                    child: Text(
                                      detail['status'],
                                      style: textPoppinsBold.copyWith(
                                        color: baseColor,
                                        fontSize: 10,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                        Text(
                          "Synopsis",
                          style: textPoppinsBold.copyWith(
                            color: baseColor,
                          ),
                        ),
                        const SizedBox(height: 10),
                        Expanded(
                          child: SingleChildScrollView(
                            child: Text(
                              detail['synopsis'] ?? 'Not Found',
                              style: textPoppins.copyWith(
                                color: baseColor,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }
}

class ButtonBackCustom extends StatelessWidget {
  const ButtonBackCustom({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        margin: const EdgeInsets.only(left: 20, top: 20),
        child: const Icon(
          Icons.arrow_back,
          size: 30,
          color: Colors.white,
        ),
      ),
    );
  }
}

class HeroImage extends StatelessWidget {
  const HeroImage({
    Key? key,
    required this.detail,
  }) : super(key: key);

  final Map<String, dynamic> detail;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 300,
      decoration: BoxDecoration(
        color: Colors.black,
        image: DecorationImage(
          opacity: 0.5,
          fit: BoxFit.cover,
          image: NetworkImage(
            detail['images']['jpg']['large_image_url'],
          ),
        ),
      ),
    );
  }
}

class ImageSmall extends StatelessWidget {
  const ImageSmall({
    Key? key,
    required this.detail,
  }) : super(key: key);

  final Map<String, dynamic> detail;

  @override
  Widget build(BuildContext context) {
    return Container(
      // margin: const EdgeInsets.only(l),
      width: 150,
      height: 200,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(
          Radius.circular(
            20,
          ),
        ),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: NetworkImage(
            detail['images']['jpg']['image_url'],
          ),
        ),
      ),
    );
  }
}
