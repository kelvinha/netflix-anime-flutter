import 'package:flutter/material.dart';
import '../theme.dart';

class CardNew extends StatelessWidget {
  final String? title;
  final String? url;

  const CardNew({Key? key, this.title, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.43,
      margin: const EdgeInsets.all(8.0),
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: primaryColor.withOpacity(0.5),
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        children: [
          Image.network(
            url!,
            fit: BoxFit.cover,
            height: 280,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            title!,
            textAlign: TextAlign.center,
            style: textPoppinsBold.copyWith(color: baseColor),
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
