class Anime {
  final int id;
  final String title;
  final String url;

  Anime({
    required this.id,
    required this.title,
    required this.url,
  });

  factory Anime.fromJson(Map<String, dynamic> json) {
    return Anime(
      id: json['mal_id'],
      title: json['title'],
      url: json['images']['jpg']['image_url'],
    );
  }
}
