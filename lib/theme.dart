import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


const Color primaryColor = Color(0xFFB02E2E);
const Color secondaryColor = Color(0xFF17202A);
const Color thirdColor = Color(0xFFF4B51E);
const Color baseColor = Color(0xFFF0F0F0);

TextStyle textPoppins = GoogleFonts.poppins(
  fontSize: 14,
  fontWeight: FontWeight.w400
);

TextStyle textPoppinsBold = GoogleFonts.poppins(
  fontSize: 14,
  fontWeight: FontWeight.w700
);